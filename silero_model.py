"""
 - conv_block
     - dw_conv
         - conv1d
         - identity
         - relu

     - pw_conv
         - conv1d
         - identity

     - relu

 - conv_block_proj
     - dw_conv
         - conv1d
         - identity
         - relu

     - pw_conv
         - conv1d
         - identity

     - conv1d
     - relu


- conv_layer_proj:
     - conv_block
     - conv_block
     - conv_block
     - conv_block
     - conv_block
     - conv_block_proj

- conv_layer:
     - conv_block
     - conv_block
     - conv_block
     - conv_block
     - conv_block
     - conv_block



 - Encoder:
     - conv_layer_proj
     - TransformerEncoderLayer
     - conv1d
     - batchnorm1d
     - relu

     - conv_layer
     - TransformerEncoderLayer
     - conv1d
     - batchnorm1d
     - relu

     - conv_layer
     - TransformerEncoderLayer
     - conv1d
     - batchnorm1d
     - relu

     - conv_layer_proj
     - TransformerEncoderLayer
     - conv1d
     - batchnorm1d
     - relu

     - conv_layer
     - TransformerEncoderLayer
     - conv1d
     - batchnorm1d
     - relu

     - conv_layer
     - TransformerEncoderLayer
     - conv1d
     - batchnorm1d
     - relu


 - decoder:
     - TransformerEncoderLayer
     - relu
     - conv1d
     - adaptiveAvgPool1d


 - model:
    - STFT
    - AdaptiveNormalization
    - encoder
    - decoder
    - sigmoid


"""
import os
import torch
import torchaudio
import numpy as np
import onnx.numpy_helper
from torch import Tensor


class MultiHeadAttention(torch.nn.Module):
    def __init__(self, in_features, out_features, n_heads, proj_in, proj_out, has_out_proj=True, scale=1, qkv_scale=1):
        super().__init__()
        self.QKV = torch.nn.Linear(in_features, out_features)
        # self.QKV = torch.nn.Identity(in_features, out_features)
        # self.QKV = torch.nn.quantized.Linear(in_features, out_features, dtype=torch.qint8)
        self.QKV.scale = qkv_scale

        self.n_heads = n_heads
        self.has_out_proj = has_out_proj
        self.out_proj = torch.nn.Linear(proj_in, proj_out)
        # self.out_proj = torch.nn.Identity(proj_in, proj_out)
        # self.out_proj = torch.nn.quantized.Linear(proj_in, proj_out, dtype=torch.float16)
        self.scale = scale

    def forward(self, x: Tensor) -> Tensor:
        bsz, seq, dim, = x.shape
        head_dim = dim // self.n_heads
        _0 = torch.chunk((self.QKV).forward(x, ), 3, -1)
        q, k, v, = _0
        _1 = torch.transpose(k, 0, 1).contiguous()
        _2 = [seq, bsz * self.n_heads, head_dim]
        k0 = torch.transpose(_1.view(_2), 0, 1)
        _3 = torch.transpose(q, 0, 1).contiguous()
        _4 = [seq, torch.mul(bsz, self.n_heads), head_dim]
        q0 = torch.transpose(_3.view(_4), 0, 1)
        _5 = torch.transpose(v, 0, 1).contiguous()
        _6 = [seq, torch.mul(bsz, self.n_heads), head_dim]
        v0 = torch.transpose(_5.view(_6), 0, 1)
        _7 = torch.matmul(k0, torch.transpose(q0, 1, 2))
        alpha = torch.nn.functional.softmax(_7 / self.scale, -1, 3, None, )
        attn = torch.matmul(alpha, v0)
        _8 = torch.transpose(attn, 0, 1).contiguous()
        attn0 = torch.transpose(_8.view([seq, bsz, dim]), 0, 1)
        if self.has_out_proj:
            attn1 = (self.out_proj).forward(attn0, )
        else:
            attn1 = attn0
        return attn1


class TransformerLayer(torch.nn.Module):
    def __init__(self,
                 atn_in_features,
                 atn_out_features,
                 atn_n_heads,
                 atn_proj_in,
                 atn_proj_out,
                 in_linear1,
                 out_linear1,
                 in_linear2,
                 out_linear2,
                 p_dropout=0.1,
                 norm=8,
                 reshape_inputs=True,
                 atn_has_out_proj=True,
                 atn_scale=1,
                 qkv_scale=1
                 ):
        super().__init__()
        self.reshape_inputs = reshape_inputs
        self.attention = MultiHeadAttention(atn_in_features,
                                            atn_out_features,
                                            atn_n_heads,
                                            atn_proj_in,
                                            atn_proj_out,
                                            atn_has_out_proj,
                                            atn_scale,
                                            qkv_scale=qkv_scale)
        self.dropout = torch.nn.Dropout(p_dropout)
        self.dropout1 = torch.nn.Dropout(p_dropout)
        self.dropout2 = torch.nn.Dropout(p_dropout)
        self.norm1 = torch.nn.LayerNorm(norm)
        self.norm2 = torch.nn.LayerNorm(norm)
        self.linear1 = torch.nn.Linear(in_features=in_linear1, out_features=out_linear1)
        # self.linear1 = torch.nn.Identity(in_features=in_linear1, out_features=out_linear1)
        # self.linear1 = torch.nn.quantized.Linear(in_features=in_linear1, out_features=out_linear1)
        self.linear2 = torch.nn.Linear(in_features=in_linear2, out_features=out_linear2)
        # self.linear2 = torch.nn.Identity(in_features=in_linear1, out_features=out_linear1)
        # self.linear2 = torch.nn.quantized.Linear(in_features=in_linear2, out_features=out_linear2)
        self.activation = torch.nn.ReLU()
        self.scale = atn_scale

    def forward(self, x: Tensor) -> Tensor:
        if self.reshape_inputs:
            x = x.permute(0, 2, 1).contiguous()

        x2 = x + self.dropout1(self.attention(x))

        x3 = self.norm1(x2)

        _2 = self.activation(self.linear1(x3))

        x20 = self.linear2(self.dropout(_2))

        x5 = self.norm2(x3 + self.dropout2(x20))

        if self.reshape_inputs:
            x5 = x5.permute(0, 2, 1).contiguous()

        return x5


class ConvBlock(torch.nn.Module):
    def __init__(self,
                 dw_in,
                 dw_out,
                 dw_kernel,
                 dw_padding,
                 pw_in,
                 pw_out,
                 pw_kernel,
                 pw_padding,
                 pw_stride=1,
                 dw_stride=1,
                 dw_groups=1,
                 pw_groups=1,
                 ):
        super().__init__()
        self.dw_conv = torch.nn.Sequential(
            torch.nn.Conv1d(in_channels=dw_in,
                            out_channels=dw_out,
                            kernel_size=dw_kernel,
                            stride=dw_stride,
                            padding=dw_padding,
                            groups=dw_groups),
            torch.nn.Identity(),
            torch.nn.ReLU()
        )

        self.pw_conv = torch.nn.Sequential(
            torch.nn.Conv1d(in_channels=pw_in,
                            out_channels=pw_out,
                            kernel_size=pw_kernel,
                            stride=pw_stride,
                            padding=pw_padding,
                            groups=pw_groups),
            torch.nn.Identity()
        )
        self.activation = torch.nn.ReLU()

    def forward(self, x: Tensor) -> Tensor:
        return self.activation(self.pw_conv(self.dw_conv(x)) + x)


class ConvBlockProj(torch.nn.Module):
    def __init__(self,
                 dw_in,
                 dw_out,
                 dw_kernel,
                 dw_padding,
                 pw_in,
                 pw_out,
                 pw_kernel,
                 pw_padding,
                 proj_in,
                 proj_out,
                 proj_kernel,
                 proj_padding,
                 proj_stride=1,
                 pw_stride=1,
                 dw_stride=1,
                 dw_groups=1,
                 pw_groups=1,
                 proj_groups=1):
        super().__init__()
        self.dw_conv = torch.nn.Sequential(
            torch.nn.Conv1d(in_channels=dw_in,
                            out_channels=dw_out,
                            kernel_size=dw_kernel,
                            stride=dw_stride,
                            padding=dw_padding,
                            groups=dw_groups),
            torch.nn.Identity(),
            torch.nn.ReLU()
        )

        self.pw_conv = torch.nn.Sequential(
            torch.nn.Conv1d(in_channels=pw_in,
                            out_channels=pw_out,
                            kernel_size=pw_kernel,
                            stride=pw_stride,
                            padding=pw_padding,
                            groups=pw_groups),
            torch.nn.Identity()
        )
        self.proj = torch.nn.Conv1d(in_channels=proj_in,
                                    out_channels=proj_out,
                                    kernel_size=proj_kernel,
                                    stride=proj_stride,
                                    padding=proj_padding,
                                    groups=proj_groups)
        self.activation = torch.nn.ReLU()

    def forward(self, x: Tensor) -> Tensor:
        residual = self.proj(x)
        return self.activation(self.pw_conv(self.dw_conv(x)) + residual)


class AdaptiveAudioNormalization(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.reflect = torch.nn.ReflectionPad1d((8, 8))
        self.filter_ = torch.nn.Parameter(torch.rand((1, 1, 17)), requires_grad=True)

    def forward(self, spect: Tensor) -> Tensor:
        spect1 = torch.log1p(spect * 2 ** 20)
        if len(spect1.shape) == 2:
            spect1 = torch.unsqueeze(spect1, 0)[:, :, :]

        mean = torch.mean(spect1, dim=1, keepdim=True, dtype=None)
        mean1 = torch.conv1d(self.reflect(mean), self.filter_)
        mean_mean = torch.mean(mean1, dim=-1, keepdim=True, dtype=None)
        return spect1 - mean_mean


class SFTFExtractor(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.hop_length = 160
        self.n_fft = 320
        self.win_length = 320
        self.mode = 'mag'

    def forward(self, wav):
        stft_sample = torch.stft(wav, self.n_fft, self.hop_length, self.win_length)
        mag, phase = torchaudio.functional.magphase(stft_sample)

        if self.mode == 'mag':
            return mag
        elif self.mode == 'phase':
            return phase
        elif self.mode == 'magphase':
            _7 = mag * torch.cos(phase)
            _8 = mag * torch.sin(phase)
            return torch.cat([_7, _8], dim=1)
        else:
            raise Exception("")


class ModifiedSileroVAD(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.feature_extractor = SFTFExtractor()
        self.adaptive_normalization = AdaptiveAudioNormalization()
        self.encoder = torch.nn.Sequential(
            torch.nn.Sequential(
                ConvBlock(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=161,
                          pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=161,
                          pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=161,
                          pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=161,
                          pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=161,
                          pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlockProj(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=64,
                              pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1, proj_out=64, proj_in=161,
                              proj_kernel=1, proj_padding=0, proj_groups=1),
            ),
            TransformerLayer(atn_in_features=64, atn_out_features=192, atn_n_heads=2, atn_proj_in=64, atn_proj_out=64,
                             in_linear1=64, out_linear1=64, in_linear2=64, out_linear2=64, p_dropout=0.1, norm=64,
                             reshape_inputs=True, atn_has_out_proj=True, atn_scale=5.656854249492381, qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=64, in_channels=64, stride=2, kernel_size=1, padding=0, groups=1, dilation=1),
            torch.nn.BatchNorm1d(64),
            torch.nn.ReLU(),
            torch.nn.Sequential(
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
            ),
            TransformerLayer(atn_in_features=64, atn_out_features=192, atn_n_heads=2, atn_proj_in=64, atn_proj_out=64,
                             in_linear1=64, out_linear1=64, in_linear2=64, out_linear2=64, p_dropout=0.1, norm=64,
                             reshape_inputs=True, atn_has_out_proj=True, atn_scale=5.656854249492381, qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=64, in_channels=64, stride=2, kernel_size=1, padding=0, groups=1, dilation=1),
            torch.nn.BatchNorm1d(64),
            torch.nn.ReLU(),
            torch.nn.Sequential(
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
            ),
            TransformerLayer(atn_in_features=64, atn_out_features=192, atn_n_heads=2, atn_proj_in=64, atn_proj_out=64,
                             in_linear1=64, out_linear1=64, in_linear2=64, out_linear2=64, p_dropout=0.1, norm=64,
                             reshape_inputs=True, atn_has_out_proj=True, atn_scale=5.656854249492381, qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=64, in_channels=64, stride=2, kernel_size=1, padding=0, groups=1, dilation=1),
            torch.nn.BatchNorm1d(64),
            torch.nn.ReLU(),
            torch.nn.Sequential(
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlockProj(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=128,
                              pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1, proj_out=128, proj_in=64,
                              proj_kernel=1, proj_padding=0, proj_groups=1),
            ),
            TransformerLayer(atn_in_features=128, atn_out_features=384, atn_n_heads=2, atn_proj_in=128,
                             atn_proj_out=128, in_linear1=128, out_linear1=128, in_linear2=128, out_linear2=128,
                             p_dropout=0.1, norm=128, reshape_inputs=True, atn_has_out_proj=True, atn_scale=8.0,
                             qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=128, in_channels=128, stride=1, kernel_size=1, padding=0, groups=1,
                            dilation=1),
            torch.nn.BatchNorm1d(128),
            torch.nn.ReLU(),
            torch.nn.Sequential(
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
            ),
            TransformerLayer(atn_in_features=128, atn_out_features=384, atn_n_heads=2, atn_proj_in=128,
                             atn_proj_out=128, in_linear1=128, out_linear1=128, in_linear2=128, out_linear2=128,
                             p_dropout=0.1, norm=128, reshape_inputs=True, atn_has_out_proj=True, atn_scale=8.0,
                             qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=128, in_channels=128, stride=1, kernel_size=1, padding=0, groups=1,
                            dilation=1),
            torch.nn.BatchNorm1d(128),
            torch.nn.ReLU(),
            torch.nn.Sequential(
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
            ),
            TransformerLayer(atn_in_features=128, atn_out_features=384, atn_n_heads=2, atn_proj_in=128,
                             atn_proj_out=128, in_linear1=128, out_linear1=128, in_linear2=128, out_linear2=128,
                             p_dropout=0.1, norm=128, reshape_inputs=True, atn_has_out_proj=True, atn_scale=8.0,
                             qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=128, in_channels=128, stride=1, kernel_size=1, padding=0, groups=1,
                            dilation=1),
            torch.nn.BatchNorm1d(128),
            torch.nn.ReLU(),
        )

        self.decoder = torch.nn.Sequential(
            TransformerLayer(atn_in_features=128, atn_out_features=384, atn_n_heads=2, atn_proj_in=128,
                             atn_proj_out=128, in_linear1=128, out_linear1=128, in_linear2=128, out_linear2=128,
                             p_dropout=0.1, norm=128, reshape_inputs=True, atn_has_out_proj=True, atn_scale=8.0,
                             qkv_scale=1.0),
            torch.nn.ReLU(),
            torch.nn.Conv1d(out_channels=2, in_channels=128, stride=1, kernel_size=1, padding=0, groups=1, dilation=1),
            torch.nn.AdaptiveAvgPool1d(1),
        )
        self.sigmoid = torch.nn.Sigmoid()

    def forward(self, x):
        x = self.feature_extractor(x)
        x = self.adaptive_normalization(x)
        x = self.encoder(x)
        x = self.decoder(x)
        x = torch.squeeze(x, -1)
        x = self.sigmoid(x)
        return x


class ModifiedWithoutSTFT(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.adaptive_normalization = AdaptiveAudioNormalization()
        self.encoder = torch.nn.Sequential(
            torch.nn.Sequential(
                ConvBlock(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=161,
                          pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=161,
                          pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=161,
                          pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=161,
                          pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=161,
                          pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlockProj(dw_out=161, dw_in=161, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=161, pw_out=64,
                              pw_in=161, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1, proj_out=64, proj_in=161,
                              proj_kernel=1, proj_padding=0, proj_groups=1),
            ),
            TransformerLayer(atn_in_features=64, atn_out_features=192, atn_n_heads=2, atn_proj_in=64, atn_proj_out=64,
                             in_linear1=64, out_linear1=64, in_linear2=64, out_linear2=64, p_dropout=0.1, norm=64,
                             reshape_inputs=True, atn_has_out_proj=True, atn_scale=5.656854249492381, qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=64, in_channels=64, stride=2, kernel_size=1, padding=0, groups=1, dilation=1),
            torch.nn.BatchNorm1d(64),
            torch.nn.ReLU(),
            torch.nn.Sequential(
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
            ),
            TransformerLayer(atn_in_features=64, atn_out_features=192, atn_n_heads=2, atn_proj_in=64, atn_proj_out=64,
                             in_linear1=64, out_linear1=64, in_linear2=64, out_linear2=64, p_dropout=0.1, norm=64,
                             reshape_inputs=True, atn_has_out_proj=True, atn_scale=5.656854249492381, qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=64, in_channels=64, stride=2, kernel_size=1, padding=0, groups=1, dilation=1),
            torch.nn.BatchNorm1d(64),
            torch.nn.ReLU(),
            torch.nn.Sequential(
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
            ),
            TransformerLayer(atn_in_features=64, atn_out_features=192, atn_n_heads=2, atn_proj_in=64, atn_proj_out=64,
                             in_linear1=64, out_linear1=64, in_linear2=64, out_linear2=64, p_dropout=0.1, norm=64,
                             reshape_inputs=True, atn_has_out_proj=True, atn_scale=5.656854249492381, qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=64, in_channels=64, stride=2, kernel_size=1, padding=0, groups=1, dilation=1),
            torch.nn.BatchNorm1d(64),
            torch.nn.ReLU(),
            torch.nn.Sequential(
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=64,
                          pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlockProj(dw_out=64, dw_in=64, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=64, pw_out=128,
                              pw_in=64, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1, proj_out=128, proj_in=64,
                              proj_kernel=1, proj_padding=0, proj_groups=1),
            ),
            TransformerLayer(atn_in_features=128, atn_out_features=384, atn_n_heads=2, atn_proj_in=128,
                             atn_proj_out=128, in_linear1=128, out_linear1=128, in_linear2=128, out_linear2=128,
                             p_dropout=0.1, norm=128, reshape_inputs=True, atn_has_out_proj=True, atn_scale=8.0,
                             qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=128, in_channels=128, stride=1, kernel_size=1, padding=0, groups=1,
                            dilation=1),
            torch.nn.BatchNorm1d(128),
            torch.nn.ReLU(),
            torch.nn.Sequential(
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
            ),
            TransformerLayer(atn_in_features=128, atn_out_features=384, atn_n_heads=2, atn_proj_in=128,
                             atn_proj_out=128, in_linear1=128, out_linear1=128, in_linear2=128, out_linear2=128,
                             p_dropout=0.1, norm=128, reshape_inputs=True, atn_has_out_proj=True, atn_scale=8.0,
                             qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=128, in_channels=128, stride=1, kernel_size=1, padding=0, groups=1,
                            dilation=1),
            torch.nn.BatchNorm1d(128),
            torch.nn.ReLU(),
            torch.nn.Sequential(
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
                ConvBlock(dw_out=128, dw_in=128, dw_kernel=5, dw_stride=1, dw_padding=2, dw_groups=128, pw_out=128,
                          pw_in=128, pw_kernel=1, pw_stride=1, pw_padding=0, pw_groups=1),
            ),
            TransformerLayer(atn_in_features=128, atn_out_features=384, atn_n_heads=2, atn_proj_in=128,
                             atn_proj_out=128, in_linear1=128, out_linear1=128, in_linear2=128, out_linear2=128,
                             p_dropout=0.1, norm=128, reshape_inputs=True, atn_has_out_proj=True, atn_scale=8.0,
                             qkv_scale=1.0),
            torch.nn.Conv1d(out_channels=128, in_channels=128, stride=1, kernel_size=1, padding=0, groups=1,
                            dilation=1),
            torch.nn.BatchNorm1d(128),
            torch.nn.ReLU(),
        )

        self.decoder = torch.nn.Sequential(
            TransformerLayer(atn_in_features=128, atn_out_features=384, atn_n_heads=2, atn_proj_in=128,
                             atn_proj_out=128, in_linear1=128, out_linear1=128, in_linear2=128, out_linear2=128,
                             p_dropout=0.1, norm=128, reshape_inputs=True, atn_has_out_proj=True, atn_scale=8.0,
                             qkv_scale=1.0),
            torch.nn.ReLU(),
            torch.nn.Conv1d(out_channels=2, in_channels=128, stride=1, kernel_size=1, padding=0, groups=1, dilation=1),
            torch.nn.AdaptiveAvgPool1d(1),
        )
        self.sigmoid = torch.nn.Sigmoid()

    def forward(self, x):
        x = self.adaptive_normalization(x)
        x = self.encoder(x)
        x = self.decoder(x)
        x = torch.squeeze(x, -1)
        x = self.sigmoid(x)
        return x

    def load_my_state_dict(self, state_dict):
        own_state = self.state_dict()
        for name, param in state_dict.items():
            if name not in own_state:
                continue
            # if isinstance(param, torch.nn.Parameter):
            own_state[name].copy_(param)

        return own_state


def run_tests(model, modified_model):
    for _ in range(100):
        x = torch.randn(32, 16000, requires_grad=True)
        y = model(x)
        y_mod = modified_model(x)
        # print(y)
        # print(y_mod)
        # print(y.shape, y_mod.shape)
        assert torch.allclose(y, y_mod)

    print('=============================\n======== TEST PASSED ========\n=============================')


def children(node):
    return list(node.children())


def validate_models(model_jit, my_model):
    jit_sd = model_jit.state_dict()
    my_sd = my_model.state_dict()
    jit_keys = set(jit_sd.keys())
    my_keys = set(my_sd.keys())
    for key in my_keys.intersection(jit_keys):
        if not torch.allclose(my_sd[key], jit_sd[key]):
            raise KeyError(f'{key} is not the same in both models')


def find_node_by_input(model, inp):
    for node in model.graph.node:
        for i in node.input:
            if inp in i:
                yield node


def find_node_by_output(model, out):
    for node in model.graph.node:
        for i in node.output:
            if out in i:
                yield node


def get_index_of_nodes(nodes):
    for node in nodes:
        yield node.name.split('_')[-1]


def get_nodes_by_index(model, idxs):
    for idx in idxs:
        for node in model.graph.node:
            if node.name.split("_")[-1] == str(idx):
                yield node


def extract_hard_weights_from_onnx():
    # find all nodes with attention or linear in them
    # get one node before
    # assert matmul
    # get the second input with 4 digits
    # check that the input digits is in the model.graph.initializer names
    # map this weight to the name
    model = onnx.load('/home/alum/vad/silero-vad/files/model.onnx')
    all_nodes = []
    all_nodes.extend(list(find_node_by_input(model, 'linear1')))
    all_nodes.extend(list(find_node_by_input(model, 'linear2')))
    all_nodes.extend(list(find_node_by_input(model, 'attention')))

    node_before_idx = [int(idx) - 1 for idx in get_index_of_nodes(all_nodes)]

    nodes_before = list(get_nodes_by_index(model, node_before_idx))

    sd = {}
    for add_node, mat_mul_node in zip(all_nodes, nodes_before):
        assert "MatMul" in mat_mul_node.name
        assert len(mat_mul_node.input[1]) == 4
        assert mat_mul_node.input[1] in [t.name for t in model.graph.initializer]
        name = add_node.input[1]

        [weight] = [t for t in model.graph.initializer if t.name == mat_mul_node.input[1]]
        tensor = torch.tensor(onnx.numpy_helper.to_array(weight))
        if 'QKV' in name:
            tensor = tensor.T
        sd[name.replace("bias", 'weight')] = tensor

    return sd


def extract_easy_weights_from_onnx():
    model = onnx.load('/home/alum/vad/silero-vad/files/model.onnx')
    sd = {}
    for weight in model.graph.initializer:
        if not weight.name.isdigit():
            sd[weight.name] = torch.tensor(onnx.numpy_helper.to_array(weight))

    return sd


def load_weights_from_files():
    files_list = [
        'encoder.26.linear2.weight.npy',
        'encoder.26.linear1.weight.npy',
        'encoder.26.attention.out_proj.weight.npy',
        'encoder.26.attention.QKV.weight.npy',

        'encoder.21.linear2.weight.npy',
        'encoder.21.linear1.weight.npy',
        'encoder.21.attention.out_proj.weight.npy',
        'encoder.21.attention.QKV.weight.npy',

        'encoder.16.linear2.weight.npy',
        'encoder.16.linear1.weight.npy',
        'encoder.16.attention.out_proj.weight.npy',
        'encoder.16.attention.QKV.weight.npy',

        'encoder.11.linear2.weight.npy',
        'encoder.11.linear1.weight.npy',
        'encoder.11.attention.out_proj.weight.npy',
        'encoder.11.attention.QKV.weight.npy',

        'encoder.6.linear2.weight.npy',
        'encoder.6.linear1.weight.npy',
        'encoder.6.attention.out_proj.weight.npy',
        'encoder.6.attention.QKV.weight.npy',

        'encoder.1.linear2.weight.npy',
        'encoder.1.linear1.weight.npy',
        'encoder.1.attention.out_proj.weight_test.npy',
        'encoder.1.attention.QKV.weight.npy',

        'decoder.0.linear2.weight.npy',
        'decoder.0.linear1.weight.npy',
        'decoder.0.attention.out_proj.weight.npy',
        'decoder.0.attention.QKV.weight.npy',
    ]

    sd = {}
    for file_name in files_list:
        full_path = os.path.join('/home/alum/Downloads/', file_name)
        weight = torch.tensor(np.load(full_path))
        weight_name = file_name.rstrip('.npy')
        if 'test' in full_path:
            weight_name = weight_name.rstrip("_test") + 't'

        if 'attention' not in weight_name and 'linear' not in weight_name:
            weight = weight.T

        sd[weight_name] = weight.T

    return sd


def update_sd_with_packed_params(sd):
    sd2 = sd.copy()
    for key, value in sd.items():
        if ('attention' in key or 'linear' in key) and 'weight' in key:
            print(f'preforming action on {key}')
            tuple_name = key.replace('weight', '_packed_params._packed_params')
            bias = sd[key.replace('weight', 'bias')]
            tuple_value = (value, bias)
            sd2[tuple_name] = tuple_value
            print(f'sd[{tuple_name}]')

            dtype_name = key.replace('weight', '_packed_params.dtype')
            dtype_value = torch.float16
            sd2[dtype_name] = dtype_value
            print(f'sd[{dtype_name}]')

    return sd2


def load_modified_silero_vad_deprecated():
    model = torch.jit.load('/home/alum/vad/silero-vad/files/model.jit')
    my_model = ModifiedSileroVAD()
    origin_sd = model.state_dict()
    origin_sd.pop('decoder_number.0.norm1.weight')
    origin_sd.pop('decoder_number.0.norm1.bias')
    origin_sd.pop('decoder_number.0.norm2.weight')
    origin_sd.pop('decoder_number.0.norm2.bias')
    origin_sd.pop('attention.attention_weights')

    sd = my_model.state_dict()
    sd.update(origin_sd)
    sd.update(extract_easy_weights_from_onnx())
    sd.update(load_weights_from_files())

    my_model.load_state_dict(sd)

    model_int8 = torch.quantization.quantize_dynamic(
        my_model,  # the original model
        {torch.nn.Linear},  # a set of layers to dynamically quantize
        dtype=torch.qint8)  # the target dtype for quantized weights

    return model_int8


def load_modified_silero_vad():
    """build the model, load the weights and quantize the model.

    Note:
        'silero-vad.pt' weights file is needed.
    """
    my_model = ModifiedSileroVAD()
    my_model.load_state_dict(torch.load('silero-vad.pt'))
    model_int8 = torch.quantization.quantize_dynamic(
        my_model,  # the original model
        {torch.nn.Linear},  # a set of layers to dynamically quantize
        dtype=torch.qint8)  # the target dtype for quantized weights

    return model_int8


def assert_layers():
    model = torch.jit.load('/home/alum/vad/silero-vad/files/model.jit')
    my_model = load_modified_silero_vad()
    x_stft = np.load('x_stft.npy')
    x_stft = torch.tensor(x_stft)

    an_res = model.adaptive_normalization(x_stft)
    my_an_res = my_model.adaptive_normalization(x_stft)
    assert torch.allclose(an_res, my_an_res)

    # conv_res

    conv_res = list(model.encoder.children())[0](an_res)
    my_conv_res = my_model.encoder[0](an_res)
    assert torch.allclose(conv_res, my_conv_res)

    input_atn = conv_res.permute(0, 2, 1).contiguous()

    # qkv
    qkv_res = list(model.encoder.children())[1].attention.QKV(input_atn)
    my_qkv_res = my_model.encoder[1].attention.QKV(input_atn)
    assert torch.allclose(qkv_res, my_qkv_res)

    # out_proj
    out_proj_res = list(model.encoder.children())[1].attention.out_proj(input_atn)
    my_out_proj_res = my_model.encoder[1].attention.out_proj(input_atn)
    assert torch.allclose(out_proj_res, my_out_proj_res)

    # attention
    out_atn = list(model.encoder.children())[1].attention(input_atn)
    my_out_atn = my_model.encoder[1].attention(input_atn)
    assert torch.allclose(out_atn, my_out_atn)

    # attention
    dropout_res = list(model.encoder.children())[1].dropout1(input_atn)
    my_dropout_res = my_model.encoder[1].dropout1(input_atn)
    assert torch.allclose(dropout_res, my_dropout_res)

    # norm1
    n1_res = list(model.encoder.children())[1].norm1(dropout_res)
    my_n1_res = my_model.encoder[1].norm1(dropout_res)
    assert torch.allclose(n1_res, my_n1_res)

    # linear1
    l1_res = list(model.encoder.children())[1].linear1(n1_res)
    my_l1_res = my_model.encoder[1].linear1(n1_res)
    assert torch.allclose(l1_res, my_l1_res)

    # activation
    act_res = list(model.encoder.children())[1].activation(l1_res)
    my_act_res = my_model.encoder[1].activation(my_l1_res)
    assert torch.allclose(act_res, my_act_res)

    # dropout
    d0_res = list(model.encoder.children())[1].dropout(act_res)
    my_d0_res = my_model.encoder[1].dropout(act_res)
    assert torch.allclose(d0_res, my_d0_res)

    # linear1
    l2_res = list(model.encoder.children())[1].linear2(d0_res)
    my_l2_res = my_model.encoder[1].linear2(d0_res)
    assert torch.allclose(l2_res, my_l2_res)

    # dropout
    d2_res = list(model.encoder.children())[1].dropout2(act_res)
    my_d2_res = my_model.encoder[1].dropout2(act_res)
    assert torch.allclose(d2_res, my_d2_res)

    # dropout
    n2_res = list(model.encoder.children())[1].norm2(n1_res + d2_res)
    my_n2_res = my_model.encoder[1].norm2(n1_res + d2_res)
    assert torch.allclose(n2_res, my_n2_res)

    # transformer
    trans_out = list(model.encoder.children())[1](conv_res)
    my_out_trans = my_model.encoder[1](conv_res)
    assert torch.allclose(trans_out, my_out_trans)

    # 2 conv
    _2_conv_res = list(model.encoder.children())[2](trans_out)
    my_2_conv_res = my_model.encoder[2](trans_out)
    assert torch.allclose(_2_conv_res, my_2_conv_res)

    # encoder
    encoder_res = model.encoder(an_res)
    my_encoder_res = my_model.encoder(an_res)
    assert torch.allclose(encoder_res, my_encoder_res)

    # decoder
    decoder_res = model.decoder(encoder_res)
    my_decoder_res = my_model.decoder(encoder_res)
    assert torch.allclose(decoder_res, decoder_res)


model_jit = torch.jit.load('/home/alum/vad/silero-vad/files/model.jit')
my_model = load_modified_silero_vad()
run_tests(model_jit, my_model)

# model = torch.jit.load('/home/alum/vad/silero-vad/files/model.jit')
# modified_model = ModifiedSileroVAD(model)
# run_tests(model, modified_model)

# from torch2trt import torch2trt
# model = torch.jit.load('/home/alum/vad/silero-vad/files/model.jit')
# model = ModifiedSileroVAD(model)
# x = torch.randn(32, 16000, requires_grad=True)
# print(model(x))
#
# # convert to TensorRT feeding sample data as input
# model_trt = torch2trt(model, [x])
# # torch.onnx.export(model, x, 'modified_model.onnx')
