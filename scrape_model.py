import torch
import re

model = torch.jit.load('/home/alum/vad/silero-vad/files/model.jit')


# model_8k = torch.jit.load('/home/alum/vad/silero-vad/files/model_8k.jit')


def get_conv_params(conv_block):
    inp = conv_block.in_channels
    out = conv_block.out_channels
    k, = conv_block.kernel_size
    stride, = conv_block.stride
    padding, = conv_block.padding
    dilation, = conv_block.dilation
    groups = conv_block.groups
    return {
        'out_channels': out,
        'in_channels': inp,
        'stride': stride,
        'kernel_size': k,
        'padding': padding,
        'groups': groups,
        'dilation': dilation
    }


def conv_params_to_code(conv_block):
    conv_params = get_conv_params(conv_block)
    code = f"torch.nn.Conv1d({','.join(f'{k}={v}' for k, v in conv_params.items())})"
    return code


def get_batch_norm_params(batchnorm):
    assert all(len(param.shape) == 1 for param in batchnorm.parameters())
    assert len({param.shape[0] for param in batchnorm.parameters()}) == 1
    featrues = next(batchnorm.parameters()).shape[0]
    code = f'torch.nn.BatchNorm1d({featrues})'
    return code


def get_transformer_params(tran):
    d = {
        'atn_in_features': tran.attention.QKV.in_features,
        'atn_out_features': tran.attention.QKV.out_features,
        'atn_n_heads': tran.attention.n_heads,
        'atn_proj_in': tran.attention.out_proj.in_features,
        'atn_proj_out': tran.attention.out_proj.out_features,
        'in_linear1': tran.linear1.in_features,
        'out_linear1': tran.linear1.out_features,
        'in_linear2': tran.linear2.in_features,
        'out_linear2': tran.linear2.out_features,
        'p_dropout': tran.dropout.p,
        'norm': tran.norm1.normalized_shape[0],
        'reshape_inputs': tran.reshape_inputs,
        'atn_has_out_proj': tran.attention.has_out_proj,
        'atn_scale': tran.attention.scale,
        'qkv_scale': tran.attention.QKV.scale
    }

    code = f"TransformerLayer({', '.join(f'{k}={v}' for k, v in d.items())})"
    return code


def get_conv_block_params(conv_block):
    dw_params = get_conv_params(list(conv_block.dw_conv.children())[0])
    pw_params = get_conv_params(list(conv_block.pw_conv.children())[0])
    if conv_block.proj is not None:
        more_params = get_conv_params(conv_block.proj)
        code = "ConvBlockProj(" \
               f"dw_out={dw_params['out_channels']}, " \
               f"dw_in={dw_params['in_channels']}, " \
               f"dw_kernel={dw_params['kernel_size']}, " \
               f"dw_stride={dw_params['stride']}, " \
               f"dw_padding={dw_params['padding']}, " \
               f"dw_groups={dw_params['groups']}, " \
               f"pw_out={pw_params['out_channels']}, " \
               f"pw_in={pw_params['in_channels']}, " \
               f"pw_kernel={pw_params['kernel_size']}, " \
               f"pw_stride={pw_params['stride']}, " \
               f"pw_padding={pw_params['padding']}, " \
               f"pw_groups={pw_params['groups']}, " \
               f"proj_out={more_params['out_channels']}, " \
               f"proj_in={more_params['in_channels']}, " \
               f"proj_kernel={more_params['kernel_size']}, " \
               f"proj_padding={more_params['padding']}, " \
               f"proj_groups={more_params['groups']})"

    else:
        code = "ConvBlock(" \
               f"dw_out={dw_params['out_channels']}, " \
               f"dw_in={dw_params['in_channels']}, " \
               f"dw_kernel={dw_params['kernel_size']}, " \
               f"dw_stride={dw_params['stride']}, " \
               f"dw_padding={dw_params['padding']}, " \
               f"dw_groups={dw_params['groups']}, " \
               f"pw_out={pw_params['out_channels']}, " \
               f"pw_in={pw_params['in_channels']}, " \
               f"pw_kernel={pw_params['kernel_size']}, " \
               f"pw_stride={pw_params['stride']}, " \
               f"pw_padding={pw_params['padding']}, " \
               f"pw_groups={pw_params['groups']})"

    return code


def get_adaptive_avg_pool(adaptive_block):
    o_size = adaptive_block.output_size
    return f"torch.nn.AdaptiveAvgPool1d({o_size})"


leafs = {
    'Conv1d': conv_params_to_code,
    'BatchNorm1d': get_batch_norm_params,
    'TransformerLayer': get_transformer_params,
    'ConvBlock': get_conv_block_params,
    'ReLU': lambda x: "torch.nn.ReLU()",
    'AdaptiveAvgPool1d': get_adaptive_avg_pool
}


def iterate_model(root, tabs=''):
    list_codes = []
    for child in root.children():
        print(tabs, end='')
        # print(child.original_name)
        if child.original_name in leafs.keys():
            code = leafs[child.original_name](child)
            list_codes.append(code)

        else:
            codes = iterate_model(child, tabs + '    ')
            list_codes.append(codes)

    return list_codes


def list_of_codes_to_sequential(codes):
    total_code = "torch.nn.Sequential(\n"
    for code in codes:
        if type(code) is list:
            if len(code) <= 0:
                continue
            current_code = list_of_codes_to_sequential(code)

        else:
            current_code = code

        total_code += '    ' + current_code + ",\n"

    return total_code + ')'


if __name__ == '__main__':
    for item in ['encoder', 'decoder']:
        print(f"=== {item} ===")
        codes = iterate_model(getattr(model, item), tabs='    ')
        with open(f'{item}.gen', 'w') as f:
            f.write(list_of_codes_to_sequential(codes))
